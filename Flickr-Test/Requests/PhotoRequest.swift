//
//  PhotoRequest.swift
//  Flickr-Test
//
//  Created by Bohdan Yankivskyi on 8/16/19.
//  Copyright © 2019 by. All rights reserved.
//

import Foundation

enum PhotoRequest: HTTPRequest {
    
    case getPhotos(apiKey: String, method: String, format: String, extras: String, safeSearch: String, callback: String, text: String)
    
    var path: String {
        switch self {
        default:
            return "/services/rest"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getPhotos:
            return .get
        }
    }
    
    var query: [String : Any]? {
        switch self {
        case .getPhotos(let apiKey, let method, let format, let extras, let safeSearch, let callback, let text):
            var query = ["api_key": apiKey, "text": text, "method": method, "format": format, "extras": extras, "safe_search": safeSearch, "nojsoncallback": callback]
//            if let method = method  {
//                query["method"] = method
//            }
            
//            if let format = format  {
//                query["format"] = format
//            }
            
//            if let extras = extras  {
//                query["extras"] = extras
//            }
            
//            if let safeSearch = safeSearch  {
//                query["safe_search"] = safeSearch
//            }
//
//            if let callback = callback  {
//                query["nojsoncallback"] = callback
//            }
            return query
            
        default:
            return nil
        }
    }
    
    var body: [String : Any]? {
        switch self {
        default:
            return nil
        }
    }
    
    var formData: [String : Any]? {
        switch self {
        default:
            return nil
        }
    }
    
    var headers: [String : Any]? {
        switch self {
        default:
            return nil
        }
    }
    
    
}
