//
//  Photo.swift
//  Flickr-Test
//
//  Created by Bohdan Yankivskyi on 11/13/19.
//  Copyright © 2019 by. All rights reserved.
//

import Foundation

struct Photo: Codable {
    
    // MARK: - Properties
    let id: String
    let owner: String?
    let secret: String?
    let server: String?
    var farm: Int?
    let title: String
    var isPublic: Int?
    var isFriend: Int?
    var isFamily: Int?
    let url: String?
    let height: Int?
    let width: Int?
    
    // MARK: - CodingKeys
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case owner = "owner"
        case secret = "secret"
        case server = "server"
        case farm = "farm"
        case title = "title"
        case isPublic = "ispublic"
        case isFriend = "isfriend"
        case isFamily = "isfamily"
        case url = "url_m"
        case height = "height_m"
        case width = "width_m"
    
    }
    
}
