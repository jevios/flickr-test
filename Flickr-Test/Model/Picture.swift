//
//  Flickr.swift
//  Flickr-Test
//
//  Created by Bohdan Yankivskyi on 11/11/19.
//  Copyright © 2019 by. All rights reserved.
//

import Foundation
import RealmSwift

class FlickrModel: Object {
    
    // MARK: - Properties
    @objc dynamic var photoID = UUID().uuidString
    @objc dynamic var title = ""
    @objc dynamic var picture: Data? = nil
    
    override static func primaryKey() -> String? {
        return "photoID"
    }

}
