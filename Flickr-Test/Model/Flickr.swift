//
//  Flickr.swift
//  Flickr-Test
//
//  Created by Bohdan Yankivskyi on 11/13/19.
//  Copyright © 2019 by. All rights reserved.
//

import Foundation

class Flickr: Codable {
    
    // MARK: - Properties
    let photos: Photos
    let stat: String
    
    // MARK: - CodingKeys
    enum CodingKeys: String, CodingKey {
        case photos = "photos"
        case stat = "stat"
    }
    
}
