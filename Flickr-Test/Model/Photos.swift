//
//  Image.swift
//  Flickr-Test
//
//  Created by Bohdan Yankivskyi on 11/13/19.
//  Copyright © 2019 by. All rights reserved.
//

import Foundation

struct Photos: Codable {
    
    // MARK: - Properties
    let page: Int
    let pages: Int
    let perpage: Int
    let total: String
    let photo: [Photo]
    
    // MARK: - CodingKeys
    enum CodingKeys: String, CodingKey {
        case page = "page"
        case pages = "pages"
        case perpage = "perpage"
        case total = "total"
        case photo = "photo"
        
    }
    
}
