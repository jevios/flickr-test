//
//  UIViewController.swift
//  Flickr-Test
//
//  Created by Bohdan Yankivskyi on 11/11/19.
//  Copyright © 2019 by. All rights reserved.
//

import UIKit

var activityIndicator : UIView?

extension UIViewController {
    func showSpinner(superView : UIView) {
        let spinnerView = UIView.init(frame: superView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            superView.addSubview(spinnerView)
        }
        
        activityIndicator = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            activityIndicator?.removeFromSuperview()
            activityIndicator = nil
        }
    }
    
    
    func presentAlertController(withTitle title: String? = nil, message: String? = nil, actionHandler handler: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel) { (action) in handler?() }
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func presentErrorAlertController(withTitle title: String? = "Ooops...", message: String? = "Something went wrong", actionHandler handler: (() -> Void)? = nil) {
        presentAlertController(withTitle: title, message: message, actionHandler: handler)
    }
    
}
