//
//  FlickrCell.swift
//  Flickr-Test
//
//  Created by Bohdan Yankivskyi on 11/10/19.
//  Copyright © 2019 by. All rights reserved.
//

import UIKit

class FlickrCell: UITableViewCell {
    
    // MARK: Properties
    static let identifier = String(describing: FlickrCell.self)
    
    private let titleLabel : UILabel = {
        let label = UILabel()
        label.text = "Title"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .left
        return label
    }()
    
    private let pictureView : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "image_placeholder")
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()
    
    
    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(pictureView)
        addSubview(titleLabel)
        
        pictureView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 5, paddingLeft: 5, paddingBottom: 5, paddingRight: 0, width: 90, height: 0, enableInsets: false)
        
        titleLabel.anchor(top: topAnchor, left: pictureView.rightAnchor, bottom: nil, right: nil, paddingTop: 40, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width / 2, height: 0, enableInsets: false)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - СonfigureCell
    func configure(with flickr: FlickrModel) {
        titleLabel.text = flickr.title
        pictureView.image = UIImage(data: flickr.picture!)
    }
    
}
