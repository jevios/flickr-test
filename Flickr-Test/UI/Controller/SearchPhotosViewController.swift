//
//  ViewController.swift
//  Flickr-Test
//
//  Created by Bohdan Yankivskyi on 11/10/19.
//  Copyright © 2019 by. All rights reserved.
//

import UIKit
import RealmSwift
import Foundation

class SearchPhotosViewController: UIViewController {
    
    // MARK: - Properties
    private let tableView = UITableView()
    private var searchText: String?
    private var pictureModel:Results<FlickrModel>?
    
    private let flickrImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "image_placeholder")
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
   private let textField = SearchTextField()
    
    // MARK: - ViewController's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        textField.delegate = self
        fetchData()
        addSubviews()
        setupLayout()
        setUpTableView()
        print(Realm.Configuration.defaultConfiguration.fileURL)
        
    }
    
    private func fetchData() {
        let realm = try! Realm()
        pictureModel = realm.objects(FlickrModel.self)
        
    }
    
    // MARK: - Private
    private func setupLayout() {
        textField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        textField.topAnchor.constraint(equalTo: view.topAnchor, constant: 40).isActive = true
        textField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        textField.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
        flickrImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        flickrImageView.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 24 ).isActive = true
        flickrImageView.heightAnchor.constraint(equalToConstant: 180).isActive = true
        flickrImageView.widthAnchor.constraint(equalToConstant: 220).isActive = true
        
        tableView.topAnchor.constraint(equalTo: flickrImageView.bottomAnchor, constant: 8 ).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
    }

    
    private func setUpTableView() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(FlickrCell.self, forCellReuseIdentifier: FlickrCell.identifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    
    private func addSubviews() {
        view.addSubview(textField)
        view.addSubview(flickrImageView)
        view.addSubview(tableView)
    }
    
    
    private func getPhoto(with text: String) {
        self.showSpinner(superView: flickrImageView)
        ServiceProvider.shared.photo.getPhotos(text: text) {  [weak self] (result) in
            DispatchQueue.main.async {
                guard let strongSelf = self else { return }
                strongSelf.removeSpinner()
                switch result {
                case .success(let flickr):
                    let photo = flickr.photos.photo.randomElement()
                    guard let photoUrl = photo?.url else { return }
                    strongSelf.fetchAndSaveImage(photoUrl)
                case .failure(let error):
                    strongSelf.presentAlertController(message: error.message, actionHandler: nil)
                }
            }
        }
        
    }

    
    private func fetchAndSaveImage(_ url: String) {
        let imageURL = URL(string: url)
        let task = URLSession.shared.dataTask(with: imageURL!) { (data, response, error) in
            if error == nil {
                let downloadImage = UIImage(data: data!)!
                DispatchQueue.main.async(){
                    self.flickrImageView.image = downloadImage
                    let flickr = FlickrModel()
                    flickr.picture = data
                    flickr.title = self.searchText!
                    let realm = try! Realm()
                    try! realm.write {
                        realm.add(flickr)
                        
                    }
                    self.tableView.reloadData()
                }
            }
        }
        
        task.resume()
    }

}

// MARK: - UITableViewDataSource
extension SearchPhotosViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pictureModel!.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FlickrCell.identifier, for: indexPath) as! FlickrCell
        cell.configure(with: pictureModel![indexPath.row])
        
        return cell
    }
    
}

// MARK: - UITableViewDelegate
extension SearchPhotosViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}


// MARK: - UITextFieldDelegate
extension SearchPhotosViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textField {
            textField.resignFirstResponder()
            searchText = textField.text
            if (searchText!.isEmpty) {
                presentAlertController(message: "Search text cannot be empty", actionHandler: nil)
            }

            getPhoto(with: searchText!)
            return false
        }
        return true
    }
}


