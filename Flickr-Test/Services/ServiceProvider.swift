//
//  ServiceProvider.swift
//  weatheropen
//
//  Created by Bohdan Yankivskyi on 8/16/19.
//  Copyright © 2019 by. All rights reserved.
//

import Foundation

final class ServiceProvider {
    
    // MARK: - Singleton
    static let shared = ServiceProvider()
    
    // MARK: - Properties
    var http: HTTPService
    var photo: PhotoService

    private var services: [ServiceInterface] {
        return [http, photo]
    }
    
    // MARK: - Initialization
    private init() {
        let environment = Environment("Dev", host: "https://api.flickr.com")
//        let environment = Environment("Dev", host: "https.api.flickr.com", path: "/services/rest")
        http = HTTPService(environment: environment)
        photo = PhotoService()
        
        for service in services {
            service.initialize()
        }
    }
    
}

