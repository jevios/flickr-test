//
//  WeatherService.swift
//  weatheropen
//
//  Created by Bohdan Yankivskyi on 8/16/19.
//  Copyright © 2019 by. All rights reserved.
//

import Foundation

class PhotoService: ServiceInterface {
    
    func getPhotos(text: String, completion handler: @escaping (Result<Flickr, GenericError>) -> Void) {
        let request = PhotoRequest.getPhotos(apiKey: Constants.FlickrAPIValues.APIKey, method: Constants.FlickrAPIValues.SearchMethod, format: Constants.FlickrAPIValues.ResponseFormat, extras: Constants.FlickrAPIValues.MediumURL, safeSearch: Constants.FlickrAPIValues.SafeSearch, callback: Constants.FlickrAPIValues.DisableJSONCallback, text: text)
        ServiceProvider.shared.http.execute(request: request) { (result: Result<Flickr, GenericError>) in
            handler(result)
        }
    }
}
